### Graphique:

Pour faire marcher la partie graphique, vous devez installer les paquets react-chartjs-2 et reactjs avec les commandes:

```
npm install react-chartjs-2
npm install react.js
```

De plus, vous devez installer typescript pour que le projet marche correctement.

```
npm install typescript
```

En cas de soucis pour la visualisation des graphiques, veuillez faire un controle + molette pour zoomer ou dézoomer.

Après modification du code, vous devez réaliser un refresh (F5) sur la page quiz pour observer les modifications.

Si l'ecran des graphiques est noir, vous devez changer la couleur du thème de votre poste pour le passer de sombre à clair.

----

### Serveur:



Aller dans le dossier "servers" et effectuer la commande:

```
./startDocker.sh
```
ou

```
docker-compose up -d
```

Afin de voir plus en précision les logs, il est conseillé de faire:

```
docker-compose up -d postgres-server pgadmin
docker-compose up node-express
```

### Adresses IP:
Application:
localhost:3000

serveur express:
localhost:8080

base de données:
localhost:8081

pgAdmin:
localhost:8082

## Mise en place de la base de données
Sur PGADMIN:
[Cliquez ici pour être redirigé sur PGADMIN](http://localhost:8082)
```
Login: admin@brainbooster.fr
MDP: admin

Clique droit sur "Servers" > "Create" > "Server..."

"name": postgres-server

Onglet connection:
host: 172.22.0.1
port: 8081
username: admin
password: admin
Cocher "Save password?"
Sauvegarder

Clique droit sur "Databases" > "Create" > "Databases"
"Database": brain-booster

Clique droit sur "brain-booster" > "Query Tool"

A l'intérieur du Query Tool, copier/coller le contenu de /servers/sql-backups/SQL-FILE.txt
Executer
```

L'application est prète à être utilisée à 100% de ses fonctionnalités.

----

### Connection Front -> Server:

Pour que l'aplication ce connect au serveur, il faudra changer l'adresse :
```
const server
```
Sur les pages :

```
src/components/connection/ConnectionContainer.tsx
src/App.tsx
```
