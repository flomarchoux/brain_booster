# Restart PWA on server
git pull;
npm install;
pkill -f /usr/bin/serve;
ionic build --prod;
npm run copy-config;
nohup serve  --ssl-cert /etc/letsencrypt/live/73k05.xyz/fullchain.pem --ssl-key /etc/letsencrypt/live/73k05.xyz/privkey.pem --single build &
