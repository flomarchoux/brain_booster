const crypto = require('crypto');
const Pool = require('pg').Pool
const pool = new Pool({
    user: 'postgres',
    host: '172.22.0.1',
    database: 'brain-booster',
    password: 'jeanpeuxplus',
    port: 8081,
});

const getClients = () => {
    return new Promise(function (resolve, reject) {
        pool.query('SELECT * FROM users ORDER BY id ASC', (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(results.rows);
        })
    })
}

const getClientLogin = (body) => {
    return new Promise(function (resolve, reject) {
        const {email, pwd} = body
        console.log('New request: ' + email + ' connected !')
        pool.query('SELECT id, email FROM users WHERE (email = $1 OR username = $1) AND password= $2', [email, pwd], (error, results) => {
            if (error) {
                reject(error)
            }
            if (results.rows.length !== 1) {
                resolve(false)
            } else {
                results.rows[0].hash = createHash();
                createCookie(results.rows[0]);
                resolve(results.rows);
            }
        })
    })
}

function createHash(){
    let current_date = (new Date()).valueOf().toString();
    let random = Math.random().toString();
    return crypto.createHash('sha1').update(current_date + random).digest('hex');
}

function createCookie(element){
    const {id, email, hash} = element
    pool.query('INSERT INTO cookies(user_id, email, hash) VALUES ($1, $2, $3)', [id, email, hash])
}

const createClient = (body) => {
    return new Promise(function (resolve, reject) {
        const {email, pwd} = body
        pool.query('INSERT INTO users(email, password) VALUES ($1, $2) RETURNING *', [email, pwd], (error, results) => {
            if (error) {
                reject(error)
            }
            resolve(`A new client has been added added: ${results.rows}`)
        })
    })
}

const setClientSave = (body) => {
    return new Promise(function (resolve, reject) {
        const {id, key, data} = body
        pool.query('SELECT user_id, key, data FROM words WHERE user_id = $1 AND key = $2', [id, key], (error, results) => {
            if (error) {
                reject(error)
            }

            if (results.rows.length === 0) {
                pool.query('INSERT INTO words(user_id, key, data) VALUES ($1, $2, $3)', [id, key, data], (error, results) => {
                    if (error) {
                        reject(error)
                    }
                    console.log("A save as been added for user " + id)
                    resolve(`A save as been added for user ${id} !`)
                })
            } else {
                pool.query('UPDATE words SET data = $3 WHERE user_id = $1 AND key = $2', [id, key, data], (error, results) => {
                    if (error) {
                        reject(error)
                    }
                    console.log("A save as been updated for user " + id)
                    resolve(`A save as been updated for user ${id} !`)
                })
            }
        })
    })
}

const setClientIDB = (body) => {
    return new Promise(function (resolve, reject) {
        const {user_id, dataidb} = body
        pool.query('SELECT user_id, dataidb FROM idb WHERE user_id = $1 and dataidb = $2', [user_id, dataidb], (error, results) => {
            if (error) {
                reject(error)
                console.log(error);
            }
            if (results.rows.length === 0) {
                pool.query('INSERT INTO idb(user_id, dataidb) VALUES ($1, $2)', [user_id, dataidb], (error, results) => {
                    if (error) {
                        reject(error)
                    }
                    console.log("A IDB save as been added for user " + user_id)
                    resolve(`A IDB save as been added for user ${user_id} !`)
                })
            } else {
                console.log("A IDB save as been canceled, exist for user " + user_id)
                resolve(`A IDB save as been canceled, exist for user ${user_id} !`)
            }
        })
    })
}

const getClientSave = (body) => {
    return new Promise(function (resolve, reject) {
        const {user_id} = body
        pool.query('SELECT * FROM words WHERE user_id = $1', [user_id], (error, results) => {
            if (error) {
                reject(error)
            }
            console.log('Data restored at connection: ' + results.rows.length + ' elements for user ' + user_id)
            resolve(results.rows);
        })
    })
}

const getClientIDB = (body) => {
    return new Promise(function (resolve, reject) {
        const {user_id} = body
        pool.query('SELECT * FROM idb WHERE user_id = $1', [user_id], (error, results) => {
            if (error) {
                reject(error)
            }
            console.log('Data IDB restored at connection: ' + results.rows.length + ' elements for user ' + user_id)
            resolve(results.rows);
        })
    })
}



module.exports = {
    getClients,
    getClientLogin,
    createClient,
    setClientSave,
    setClientIDB,
    getClientSave,
    getClientIDB,
}