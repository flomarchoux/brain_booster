-- Table: public.words

-- DROP TABLE public.words;

CREATE TABLE public.words
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    user_id integer NOT NULL,
    key text COLLATE pg_catalog."default" NOT NULL,
    data text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT words_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.words
    OWNER to postgres;