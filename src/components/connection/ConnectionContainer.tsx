import React from "react";
import {
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonItem
} from "@ionic/react";
import Cookies from "universal-cookie"
import './ConnectionContainer.css';
import ReactDOM from "react-dom";

const server = "http://192.168.1.46:8080";
const cookies = new Cookies();

interface PropsConstruct {
    name: string;
}

class ConnectionContainer2 extends React.Component {

    constructor(props: PropsConstruct) {
        super(props);
        this.state = {email: ''};
        this.state = {pwd: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event: { target: { value: any; name: any; }; }) {
        const value = event.target.value;
        this.setState({
            ...this.state,
            [event.target.name]: value
        })
    }

    handleSubmit(event: { preventDefault: () => void; }) {
        // @ts-ignore
        let email: any;
        let pwd: any;
        // @ts-ignore
        email = this.state.email;
        // @ts-ignore
        pwd = this.state.pwd;

        let login = fetch(server + "/login", {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({email, pwd})
        })
            .then(response => {
                // @ts-ignore
                return (response.text());
            })

        // @ts-ignore
        function IndexedDB(value) {
            const request = indexedDB.open("Disc", 1);
            request.onupgradeneeded = (event) => {
                //@ts-ignore
                let db = event.target.result;
                let store = db.createObjectStore("FileStorage", {keyPath: "path", autoIncrement: true})
                let index = store.createIndex("by_folder", "folder")
            }
            request.onerror = (event) => {};
            request.onsuccess = (event) => {
                //@ts-ignore
                const db = event.target.result;
                setValuesToDb(db, value)
            };
        }

        //@ts-ignore
        function setValuesToDb(db, value) {
            const txn = db.transaction("FileStorage", "readwrite");
            const objectStore = txn.objectStore("FileStorage");
            let query = objectStore.put(value)
            //@ts-ignore
            query.onsuccess = function (event) {
            }

            //@ts-ignore
            query.onerror = function (event) {
            }
            txn.oncomplete = function () {
                db.close();
            }
        }

        let printLogin = () => {
            login.then((user) => {
                //@ts-ignore
                if (user !== 'false') {
                    let userjson = JSON.parse(user);
                    //@ts-ignore
                    cookies.set("brain_booster_id", userjson[0].id, {path: "/"});
                    //@ts-ignore
                    cookies.set("brain_booster_email", userjson[0].email, {path: "/"});
                    //@ts-ignore
                    cookies.set("brain_booster_hash", userjson[0].hash, {path: "/"});
                    let user_id = cookies.get("brain_booster_id");
                    let getSave = fetch(server + "/getClientSave", {
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json',
                        },
                        body: JSON.stringify({user_id})
                    })
                        .then(response => {
                            return (response.text());
                        })
                    //@ts-ignore
                    getSave.then((save) => {
                        let data = JSON.parse(save)
                        let length = data.length
                        for (let i = 0; i < length; i++) {
                            if (localStorage.getItem(data[i].key) !== undefined) {
                                localStorage.setItem(data[i].key, data[i].data)
                            }
                        }
                        let getIdb = fetch(server + "/getClientIDB", {
                            method: 'POST',
                            headers: {
                                'content-type': 'application/json',
                            },
                            body: JSON.stringify({user_id})
                        })
                            .then(response => {
                                return (response.text());
                            })

                        //@ts-ignore
                        getIdb.then((idb) => {
                            let data = JSON.parse(idb)
                            let length = data.length;
                            for (let i = 0; i < length; i++) {
                                IndexedDB(JSON.parse(data[i].dataidb));
                            }

                        })
                    })

                    ReactDOM.render(<><h1>Wait a moment please...</h1></>, document.getElementById('root'));
                    //@ts-ignore
                    setTimeout(redirect, 6000)

                    //@ts-ignore
                    function redirect() {
                        //@ts-ignore
                        window.location = "/";
                    }
                } else {
                    //@ts-ignore
                    alert("Mauvais identifiant ou mot de passe")
                }
            })
        }
        printLogin();

        event.preventDefault();

    }

    render() {
        return (
            <div className="learn-random-container">
                <IonCard className="connexion-container-ion-card">
                    <IonCardHeader>
                        <IonCardTitle color="primary">{('CONNECTION')}</IonCardTitle>
                    </IonCardHeader>
                    <form onSubmit={this.handleSubmit}>
                        <IonCardContent>
                            <IonItem>
                                email :
                                <input type="email"
                                       value={
                                           // @ts-ignore
                                           this.state.email
                                       }
                                       onChange={
                                           this.handleChange}
                                       name="email"/>
                            </IonItem>
                            <IonItem>
                                Mot de Passe :
                                <input type="password"
                                       value={
                                           // @ts-ignore
                                           this.state.pwd
                                       }
                                       onChange={this.handleChange}
                                       name="pwd"/>
                            </IonItem>
                        </IonCardContent>
                        <IonButton className="ion-connect-button" color="primary" expand="block"
                                   type="submit">Connect
                        </IonButton>
                    </form>
                </IonCard>
            </div>
        );
    }
}

export default ConnectionContainer2