import React from 'react';
import './LearnLexiconContainer.css';
import './LearnBrainBoosterContainer.css';
import {
    CreateAnimation,
    IonButton,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonCol,
    IonGrid,
    IonRow,
    useIonViewDidEnter
} from "@ionic/react";
import {BrainBooster} from '../../hooks/BrainBooster';
import {GenerateApiWord} from "../../hooks/GenerateApiWord";
import {ScreenOrientationLocker} from "../../hooks/ScreenOrientationLocker";

//Suppress this line is just for params ex
interface ContainerProps {
    lmslexicon: string
}

const LearnBrainBoosterContainer: React.FC<ContainerProps> = ({lmslexicon}) => {
    //Refresh data if page is reactivated, in case new words have been generated and lexicon was empty or session finished
    useIonViewDidEnter(() => {
        initBrainBooster()
    });

    // Declare a new state variable, which we'll call "count"
    const {word, quiz, animation, loadNextWord, checkAnswer, initBrainBooster} = BrainBooster(lmslexicon)
    const {saveLexicon} = GenerateApiWord()
    saveLexicon(lmslexicon, initBrainBooster)
    ScreenOrientationLocker().lockOrientation()
    return (
        <div className="learn-random-container">

            <CreateAnimation
                duration={700}
                iterations={1}
                onFinish={{callback: () => loadNextWord()}}
                ref={animation?.ref}>

                <IonCard className="ion-card">
                    <img className="img-card" src={word?.photo?.base64} alt="Word to learn"/>
                    <IonCardHeader>
                        <IonCardTitle>
                            {word?.quiz}
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <div className="word-definition">
                            {word?.definition}
                            {lmslexicon !== null && lmslexicon && (!quiz || !quiz?.wordKeys || quiz?.wordKeys.length <= 0) ?
                                <IonGrid>
                                    <IonRow>
                                        <IonCol>
                                            <img className="img-tuto" crossOrigin="anonymous"
                                                 src={require("../../resources/images/lexicon-tuto1.png")}
                                                 alt="Go to Vocab & Themes"/>
                                        </IonCol>

                                        <IonCol>
                                            <img className="img-tuto" crossOrigin="anonymous"
                                                 src={require("../../resources/images/lexicon-tuto2.png")}
                                                 alt="Load words in Lexicon"/>
                                        </IonCol>
                                    </IonRow>
                                </IonGrid>
                                : null}

                        </div>
                    </IonCardContent>

                    {quiz && quiz?.wordKeys && quiz?.wordKeys.length > 0 ?
                        <IonGrid className="learn-lexicon-container-bottom">
                            <IonRow>
                                <IonCol>
                                    <IonButton className={quiz?.quizButtonCssList[0]} color="primary" expand="block"
                                               disabled={quiz?.quizButtonDisableList[0]}
                                               onClick={() => {
                                                   checkAnswer(0)
                                               }}>{word?.wordRandomList?.[0]}</IonButton>
                                </IonCol>
                                <IonCol>
                                    <IonButton className={quiz?.quizButtonCssList[1]} color="primary"
                                               expand="block"
                                               disabled={quiz?.quizButtonDisableList[1]}
                                               onClick={() => {
                                                   checkAnswer(1)
                                               }}>{word?.wordRandomList?.[1]}</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    <IonButton className={quiz?.quizButtonCssList[2]} color="primary" expand="block"
                                               disabled={quiz?.quizButtonDisableList[2]}
                                               onClick={() => {
                                                   checkAnswer(2)
                                               }}>{word?.wordRandomList?.[2]}</IonButton>
                                </IonCol>
                                <IonCol>
                                    <IonButton className={quiz?.quizButtonCssList[3]}
                                               expand="block" disabled={quiz?.quizButtonDisableList[3]} onClick={() => {
                                        checkAnswer(3)
                                    }}>{word?.wordRandomList?.[3]}
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                            <div className="progress-tips"><i>Vous pouvez quitter Brain Booster à tout moment, votre
                                progression sera
                                enregistrée</i></div>
                        </IonGrid>
                        : null}
                </IonCard>
            </CreateAnimation>
        </div>
    );
};

export default LearnBrainBoosterContainer;
