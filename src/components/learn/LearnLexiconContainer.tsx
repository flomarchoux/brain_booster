import React from 'react';
import './LearnLexiconContainer.css';
import {
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardTitle,
    IonCol,
    IonGrid,
    IonIcon,
    IonRow,
    useIonViewDidEnter
} from "@ionic/react";
import {GetLexiconCard} from '../../hooks/GetLexiconCard';
import {caretBack, caretForward, flameOutline} from "ionicons/icons";
import {GenerateApiWord} from "../../hooks/GenerateApiWord";

//Suppress this line is just for params ex
interface ContainerProps {
    lmslexicon: string
}

const LearnLexiconContainer: React.FC<ContainerProps> = ({lmslexicon}) => {
    //Refresh data if page is reactivated, in case new words have been generated and lexicon was empty or session finished
    useIonViewDidEnter(() => {
        requestStorage()
    });

    const {requestApi} = GenerateApiWord()
    const {wordIndex, setWordIndex, word, wordList, updateWordFromIndex, setWord, requestStorage, isLms} = GetLexiconCard(lmslexicon)
    return (
        <div className="learn-random-container">

            <IonCard className="ion-card">
                <img className="img-card" crossOrigin="anonymous" src={word?.photo?.base64} alt={word?.spelling}/>
                <IonCardHeader>
                    <IonCardTitle>
                        {wordList?.length && wordList?.length > 0 ?
                            wordIndex + " : " : null}
                        {word?.spelling}
                    </IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    <div className="learn-word-definition">
                        {word?.definition}
                    </div>
                    {lmslexicon !== null && lmslexicon && (!wordList?.length || wordList?.length < 1) ?
                        <IonGrid>
                            <IonRow>
                                <IonCol>
                                    <img crossOrigin="anonymous"
                                         src={require("../../resources/images/lexicon-tuto1.png")}
                                         alt="Go to Vocab & Themes"/>
                                </IonCol>

                                <IonCol>
                                    <img crossOrigin="anonymous"
                                         src={require("../../resources/images/lexicon-tuto2.png")}
                                         alt="Load words in Lexicon"/>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                        : null}
                </IonCardContent>

                <IonGrid className="learn-lexicon-container-bottom">
                    <IonRow>
                        {wordList?.length && wordList?.length > 1 ?
                            <IonCol>
                                <IonIcon className="learn-lexicon-icon" icon={caretBack} onClick={
                                    () => {
                                        const next: number = wordIndex - 1 < 0 ? wordList ? wordList?.length - 1 : 0 : wordIndex - 1
                                        setWordIndex(next)
                                        updateWordFromIndex(next)
                                    }
                                }/>
                            </IonCol>
                            : null}

                        {!isLms ?
                            <IonCol>
                                <IonIcon className="learn-lexicon-icon" icon={flameOutline} onClick={
                                    () => {
                                        requestApi().then(randomWord => {
                                            setWord(randomWord)
                                        })
                                    }
                                }/>
                            </IonCol>
                            : null}

                        {wordList?.length && wordList?.length > 1 ?
                            <IonCol>
                                <IonIcon className="learn-lexicon-icon" icon={caretForward} onClick={
                                    () => {
                                        const next: number = wordIndex + 1 === wordList?.length ? 0 : wordIndex + 1;
                                        setWordIndex(next);
                                        updateWordFromIndex(next);
                                    }
                                }/>
                            </IonCol>
                            : null}
                    </IonRow>
                </IonGrid>
            </IonCard>
        </div>
    );
};

export default LearnLexiconContainer;
