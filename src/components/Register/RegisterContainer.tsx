import React from "react";
import {IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonItem} from "@ionic/react";
import "./RegisterContainer.css"
import Cookies from "universal-cookie"

const server = "http://192.168.1.46:8080";
const cookies = new Cookies();


interface PropsConstruct {
    name: string;
}

class RegisterContainer extends React.Component {

    constructor(props: PropsConstruct) {
        super(props);
        this.state = {email: ''};
        this.state = {pwd: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event: { target: { value: any; name: any; }; }) {
        const value = event.target.value;
        this.setState({
            ...this.state,
            [event.target.name]: value
        })
    }

    handleSubmit(event: { preventDefault: () => void; }) {
        // @ts-ignore
        let email: any;
        let pwd: any;
        // @ts-ignore
        email = this.state.email;
        // @ts-ignore
        pwd = this.state.pwd;
        fetch(server + "/register", {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({email, pwd})
        })
            .then(response => {
                // @ts-ignore
                let login = fetch(server + "/login", {
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify({email, pwd})
                })
                    .then(response => {
                        // @ts-ignore
                        return (response.text());
                    })
                let printLogin = () => {
                    login.then((user) => {
                        //@ts-ignore
                        if (user !== 'false') {
                            let userjson = JSON.parse(user);
                            //@ts-ignore
                            cookies.set("brain_booster_id", userjson[0].id, {path: "/"});
                            //@ts-ignore
                            cookies.set("brain_booster_email", userjson[0].email, {path: "/"});
                            //@ts-ignore
                            cookies.set("brain_booster_hash", userjson[0].hash, {path: "/"});
                            //@ts-ignore
                            window.location = "/"
                        } else {
                            //@ts-ignore
                            alert("Identifiant déjà pris")
                        }
                    })
                }
                printLogin();
            })
        event.preventDefault();
    }

    render() {
        return (
            <div className="learn-random-container">
                <IonCard className="connexion-container-ion-card">
                    <IonCardHeader>
                        <IonCardTitle color="primary">{('REGISTER')}</IonCardTitle>
                    </IonCardHeader>
                    <form onSubmit={this.handleSubmit}>
                        <IonCardContent>
                            <IonItem>
                                email :
                                <input type="email"
                                       value={
                                           // @ts-ignore
                                           this.state.email
                                       }
                                       onChange={
                                           this.handleChange}
                                       name="email"/>
                            </IonItem>
                            <IonItem>
                                Mot de Passe :
                                <input type="password"
                                       value={
                                           // @ts-ignore
                                           this.state.pwd
                                       }
                                       onChange={this.handleChange}
                                       name="pwd"/>
                            </IonItem>
                        </IonCardContent>
                        <IonButton className="ion-sign-button" color="primary" expand="block"
                                   type="submit">Register
                        </IonButton>
                    </form>
                </IonCard>
            </div>
        );
    }
}

// @ts-ignore
export default RegisterContainer