import Cookies from "universal-cookie"
const cookies = new Cookies();


export default function CookieController (){
    let cookie_id = cookies.get("brain_booster_id");
    let cookie_email = cookies.get("brain_booster_email");
    let cookie_hash = cookies.get("brain_booster_hash");
    if(cookie_id !== undefined && cookie_email !== undefined && cookie_hash !== undefined){
        return true;
    }
    return false;
};