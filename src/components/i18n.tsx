import i18n from 'i18next';
import LanguageDetector from "i18next-browser-languagedetector";
import {initReactI18next} from "react-i18next";

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        // we init with resources
        resources: {
            en: {
                translations: {}
            },
            fr: {
                translations: {
                    email: "Email",
                    password: "Mot de Passe",
                    connection: "Connexion",
                    connect: "Connection",
                    sign: "registration",
                    randomGenerate: "Apprendre",
                    memorize: "Memory Box",
                    quiz: "Brain Booster",
                    bw: "***",
                    notificationTitle: "Boost My Brain",
                    notificationContent: "It's time to Learn Break my Space Cards",
                }
            }
        },
        fallbackLng: "fr",
        debug: false,

        // have a common namespace used around the full app
        ns: ["translations"],
        defaultNS: "translations",

        keySeparator: false, // we use content as keys

        interpolation: {
            escapeValue: false
        }
    })

export default i18n
