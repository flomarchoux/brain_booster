import React from 'react';
import './ConnectionContainer.css';
import {IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonInput, IonItem} from "@ionic/react";
import {LocalNotification} from '../hooks/LocalNotification';

import {useTranslation} from "react-i18next";
import {UpdateLexiconFromApi} from "../hooks/UpdateLexiconFromApi";

interface ContainerProps {
    name: string;
}

const ConnectionContainer: React.FC<ContainerProps> = ({name}) => {

    const {t} = useTranslation();
    const {scheduleNotification} = LocalNotification();
    const {requestApiForLexicon} = UpdateLexiconFromApi();
    return (
        <div className="learn-random-container">

            <IonCard className="connexion-container-ion-card">
                <img src='/assets/icon/icon.png' alt={t('bw')}/>
                <IonCardHeader>
                    <IonCardTitle color="primary">{t('connection')}</IonCardTitle>
                </IonCardHeader>
                <IonCardContent>
                    <IonItem>
                        <IonInput color="primary" placeholder={t('email')} type="email"></IonInput>
                    </IonItem>

                    <IonItem>
                        <IonInput color="primary" placeholder={t('password')} type="password"></IonInput>
                    </IonItem>
                </IonCardContent>
                <IonButton className="ion-connect-button" color="primary" expand="block" onClick={() => {
                    scheduleNotification(t("notificationTitle"), t("notificationContent"))
                }}>{t('connect')}</IonButton>
                <IonButton className="ion-sign-button" color="primary" expand="block"
                           onClick={() => {
                               requestApiForLexicon()
                           }}>{t('sign')}</IonButton>
            </IonCard>
        </div>
    );
};

export default ConnectionContainer;
