import React, {useEffect, useState} from 'react';
import {WordApi} from "./GenerateApiWord"

import {StoreData} from "./StoreData";
import {CreateAnimation} from "@ionic/react";
import userDialogList from '../resources/json/userdialog.json';

export interface Quiz {
    wordKeys: string[]
    quizButtonDisableList: boolean[]
    quizButtonCssList: string[]
}

export interface QuizAnimation {
    ref: React.RefObject<CreateAnimation>
}

let indexQuiz: number = 0
let stopAnimation = true
const animationOutKeyFrames = [
    {offset: 0, transform: 'perspective(50rem) rotateX(0deg) rotateY(0deg)'},
    {offset: 0.5, transform: 'perspective(50rem) rotateX(0deg) rotateY(-45deg)'},
    {offset: 1, transform: 'perspective(50rem) rotateX(0deg) rotateY(-90deg)'}
]
const animationInKeyFrames = [
    {offset: 0, transform: 'perspective(50rem) rotateX(0deg) rotateY(90deg)'},
    {offset: 0.5, transform: 'perspective(50rem) rotateX(0deg) rotateY(45deg)'},
    {offset: 1, transform: 'perspective(50rem) rotateX(0deg) rotateY(0deg)'}
]

export function BrainBooster(lmsLexiconWordList: string) {

    useEffect(() => {
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const [word, setWord] = useState<WordApi>();
    const [quiz, setQuiz] = useState<Quiz>();
    const [animation, setAnimation] = useState<QuizAnimation>();

    // Get words in storage
    const {
        getObject,
        storeObject,
        getValidWordsInLexicon,
        getIndexCurrentSession,
        storeIndexCurrentSession
    } = StoreData();

    const animationRef: React.RefObject<CreateAnimation> = React.createRef()

    const initBrainBooster = async () => {
        indexQuiz = await getIndexCurrentSession()
        // your post layout code (or 'effect') here.
        requestStorage().then(wordlistSize => {
            if (!wordlistSize || wordlistSize <= 0) {
                let wordLoadLexiconError = userDialogList.lexiconEmpty
                if (lmsLexiconWordList !== "lms-lexicon-empty") {
                    wordLoadLexiconError.definition = "Click fire to generate cards in Memory Box"
                }
                setWord(wordLoadLexiconError)
            }
        })
    }

    const requestStorage = async () => {
        try {
            //Animation
            setAnimation({ref: animationRef})
            const wordKeys = await getValidWordsInLexicon()
            setQuizObject(
                wordKeys,
                [false, false, false, false],
                ["ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button"])

            setNextWord(wordKeys)

            return wordKeys.length
        } catch (e) {
            console.error("Error storage");
            return 0
        }
    };

    /**
     * Loop on words and set the next to be learned
     * @param wordKeys
     */
    const setNextWord = async (wordKeys: string[]) => {
        if (wordKeys === undefined || wordKeys.length <= 0) {
            console.error("WordKeys undefined or empty lexicon")
            return
        }

        while (indexQuiz < wordKeys?.length) {
            const wordToLearn = await getObject(wordKeys[indexQuiz])

            //Decrement session index
            let brainBoosterIndexSession = wordToLearn.brainBoosterIndexSession - 1

            //iF WORD has to be learned during the current session sessionIndex=0
            if (brainBoosterIndexSession <= 0) {
                setWord(wordToLearn)
                setQuizObject(
                    wordKeys,
                    [false, false, false, false],
                    ["ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button"])
                wordToLearn.brainBoosterSessionLearned = true
                //Store session index decremented & word has been learned during this session
                storeObject(wordToLearn.spelling, wordToLearn)
                return
            } else {
                wordToLearn.brainBoosterIndexSession = brainBoosterIndexSession
                // wordToLearn.brainBoosterSessionLearned = false
                //Store session index decremented & word has NOT been learned during this session
                storeObject(wordToLearn.spelling, wordToLearn)
            }
            //When all words has been checked in this session, back to index 0 and quit loop
            if (incrementIndex(wordKeys.length) === 0) {
                break;
            }
        }
        setSessionFinished(wordKeys)
    }

    const incrementIndex = (wordListSize: number = 0) => {
        indexQuiz = indexQuiz + 1 >= wordListSize ? 0 : indexQuiz + 1
        storeIndexCurrentSession(indexQuiz)
        return indexQuiz
    }

    /**
     * Load next card and Start in animation
     */
    const loadNextWord = async () => {
        //This callback is called mutliple times after animation is finished
        if (stopAnimation || quiz === undefined) {
            return
        }
        stopAnimation = true;

        const wordKeys = await getValidWordsInLexicon()

        //When all words has been checked in this session, back to index 0 and quit loop
        if (indexQuiz === 0) {
            await setSessionFinished(wordKeys)
        } else {
            await setNextWord(wordKeys)
        }

        let animationDom = animation?.ref.current?.animation

        animationDom?.stop()
        animationDom?.keyframes(animationInKeyFrames)
        animationDom?.play()
    };

    /**
     * When a session has been completed
     * @param wordKeys
     */
    const setSessionFinished = async (wordKeys: string[]) => {
        let hasLearnedSomething = false, i = 0
        while (i < wordKeys?.length) {
            const word = await getObject(wordKeys[i])
            hasLearnedSomething = word.brainBoosterSessionLearned || hasLearnedSomething
            //Reset learned for next session
            word.brainBoosterSessionLearned = false
            storeObject(word.spelling, word)
            i++
        }

        // If we reach this code block all card have been checked and none should be learned for this session
        setWord({
            spelling: "Session complete",
            definition: hasLearnedSomething ? "Stand back & relax" : "No word to learn for this session, refresh or add more words to your lexicon",
            photo: {
                base64: '/assets/icon/icon.png',
                filePath: "",
                url: ""
            },
            wordRandomList: [],
            quiz: "Booster terminé",
            brainBoosterScore: 1,
            brainBoosterIndexSession: 1,
            brainBoosterSessionLearned: false,
            error: -1
        })
        setQuizObject(
            [],
            [true, true, true, true],
            ["ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button", "ion-quiz-pink-button"])

        storeIndexCurrentSession(0)
    }

    /**
     * Check answer and start out animation
     * @param wordAnsweredIndex
     */
    const checkAnswer = async (wordAnsweredIndex: number) => {
        let animationDom = animation?.ref.current?.animation
        if (quiz === undefined || word === undefined) {
            return
        }
        //Correct answer
        if (word?.spelling === word?.wordRandomList?.[wordAnsweredIndex]) {
            quiz.quizButtonCssList[wordAnsweredIndex] = "ion-quiz-green-button"
            setQuizObject(quiz.wordKeys, quiz.quizButtonDisableList, quiz.quizButtonCssList)

            increaseMemoryScore(word)
            incrementIndex(quiz.wordKeys.length)

            animationDom?.stop()
            animationDom?.keyframes(animationOutKeyFrames)
            stopAnimation = false
            animationDom?.play()
        }
        //Incorrect answer
        else {
            quiz.quizButtonDisableList[wordAnsweredIndex] = true

            decreaseMemoryScore(word);

            setQuizObject(quiz.wordKeys, quiz.quizButtonDisableList, quiz.quizButtonCssList)
        }
    };

    /**
     * Util fonction to set the quiz object
     * @param wordKeys
     * @param quizButtonDisableList
     * @param quizButtonCssList
     */
    const setQuizObject = (wordKeys?: string[],
                           quizButtonDisableList?: boolean[],
                           quizButtonCssList?: string[]) => {

        if (wordKeys === undefined || quizButtonDisableList === undefined || quizButtonCssList === undefined)
            return

        setQuiz({
            wordKeys: wordKeys,
            quizButtonDisableList: quizButtonDisableList,
            quizButtonCssList: quizButtonCssList
        })
    }

    const decreaseMemoryScore = (word: WordApi) => {
        const score: number = Math.max((word?.brainBoosterScore || 0) - 1, 0)
        updateMemoryScore(word, score)
    }

    const increaseMemoryScore = (word: WordApi) => {
        const score: number = (word?.brainBoosterScore || 0) + 1
        updateMemoryScore(word, score)
    }

    const updateMemoryScore = (word: WordApi, score: number) => {
        word.brainBoosterScore = score
        word.brainBoosterIndexSession = score
        storeObject(word.spelling, word).then(r => {
        })
    }

    return {
        word,
        quiz,
        animation,
        requestStorage,
        checkAnswer,
        loadNextWord,
        initBrainBooster
    };
}
