import {useEffect, useState} from 'react';
import {GenerateApiWord, WordApi} from "./GenerateApiWord"

import {StoreData} from "./StoreData";
import {CacheFile} from "./CacheFile";
import userDialogList from "../resources/json/userdialog.json";

export interface WordList {
    words: string[];
    length: number;
}

export function GetLexiconCard(lmsLexiconWordList: string) {

    const [word, setWord] = useState<WordApi>()
    const [wordList, setWordList] = useState<WordList>()

    //    Save word in storage
    const {
        getObject,
        getValidWordsInLexicon,
        storeIndexCurrentLearning,
        getIndexCurrentLearning,
        getIsLmsEmbeddedApp
    } = StoreData();

    // Get picture in cache
    const {
        getPictureInCache
    } = CacheFile();

    // Declare a new state variable, which we'll call "count"
    const [wordIndex, setWordIndex] = useState(0)
    const [isLms, setIsLms] = useState(false)

    const {requestApi} = GenerateApiWord()

    useEffect(() => {

            getIsLmsEmbeddedApp().then((isLmsRet) => {
                setIsLms(isLmsRet)
            })

            getIndexCurrentLearning().then((index) => {
                setWordIndex(index)
            })
            // your post layout code (or 'effect') here.
            requestStorage().then(lexiconSize => {

                if (lexiconSize <= 0) {
                    if (lmsLexiconWordList !== null && lmsLexiconWordList !== undefined && lmsLexiconWordList === "lms-lexicon-empty") {
                        let wordLoadLexiconError = userDialogList.lexiconEmpty
                        setWord(wordLoadLexiconError)
                    } else {
                        requestApi().then(randomWord => {
                            setWord(randomWord)
                        })
                    }
                }
            })
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const requestStorage = async () => {
        try {
            const wordKeys = await getValidWordsInLexicon()

            if (wordKeys.length <= 0) {
                return 0
            }

            const newWord = await getObject(wordKeys[await getIndexCurrentLearning()]);

            setWord(newWord)

            setWordList({
                length: wordKeys.length,
                words: wordKeys
            })
            return wordKeys.length
        } catch (e) {
            console.error("Error storage: ", e);
            return 0
        }
    };

    const updateWordFromIndex = async (nextIndex: number) => {
        const newWord = await getObject(wordList ? wordList?.words[nextIndex] : "");
        const pictureFile = await getPictureInCache(wordList ? wordList?.words[nextIndex] : "");
        newWord.photo["base64"] = `data:image/jpeg;base64,${pictureFile.data}`

        setWord(newWord)

        storeIndexCurrentLearning(nextIndex)
    }

    return {
        word,
        wordList,
        setWord,
        requestStorage,
        updateWordFromIndex,
        wordIndex, setWordIndex,
        isLms
    };
}