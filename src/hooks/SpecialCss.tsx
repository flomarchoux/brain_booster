import {useEffect, useState} from 'react';

import {StoreData} from "./StoreData";


export function SpecialCss() {
    const [cssSpecial, setCssSpecial] = useState("");

    // Get words in storage
    const {
        getIsLmsEmbeddedApp
    } = StoreData();

    useEffect(() => {
            getIsLmsEmbeddedApp().then((isLms) => {
                if (isLms) {
                    setCssSpecial("ion-tab")
                }
            })
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    return {
        cssSpecial
    };
}
