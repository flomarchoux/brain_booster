import {useEffect} from 'react'
import {Plugins} from '@capacitor/core'
import {notificationTimeHour} from '../resources/json/Constant.json'

const {LocalNotifications} = Plugins;

// Initialize Firebase

export function LocalNotification() {
    useEffect(() => {
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const scheduleNotification = async (notificationTitle: string, notificationText: string) => {
        // your post layout code (or 'effect') here.
        if (LocalNotifications.requestPermissions) {
            LocalNotifications.requestPermissions().then(async r => {
                const notifEnabled = await LocalNotifications.areEnabled()
                if (!notifEnabled || !notifEnabled.value === true || (JSON.stringify(r) !== '{}' && r.results !== undefined && r.results[0] !== "granted")) {
                    console.error("Notification Error: permissions not granted")
                    return
                }

                //If hour of notification is passed, schedule it for tomorrow
                const notificationShowDate = new Date()
                if (notificationShowDate.getHours() > notificationTimeHour) {
                    notificationShowDate.setDate(notificationShowDate.getDate() + 1)
                }
                notificationShowDate.setHours(notificationTimeHour, 0, 0)
                let notificationObject =
                    {
                        title: notificationTitle,
                        body: notificationText,
                        id: 0,
                        schedule: {at: notificationShowDate},
                        smallIcon: "splash",
                        iconColor: "#E9476F",
                        actionTypeId: "",
                        extra: "Extra Data"
                    }
                //Plain 7days of notifications
                let notificationList = []
                notificationList.push(notificationObject)
                for (let i: number = 1; i < 8; i++) {
                    let notificationObject2 = JSON.parse(JSON.stringify(notificationObject))
                    let notificationShowDate2 = new Date(notificationShowDate)
                    notificationShowDate2.setDate(notificationShowDate2.getDate() + i)
                    notificationObject2.schedule = {at: notificationShowDate2}
                    notificationObject2.id = i
                    notificationList.push(notificationObject2)
                }
                LocalNotifications.schedule({
                    notifications: notificationList
                }).then(r => {
                    LocalNotifications.addListener("localNotificationActionPerformed", notificationAction => {
                        console.log("localNotificationActionPerformed: ", notificationAction)
                    })
                    LocalNotifications.addListener("localNotificationReceived", notificationAction => {
                        console.log("localNotificationActionPerformed: ", notificationAction)
                    })
                }).catch(reason => {
                    console.error("Notification error: ", reason)
                });
            }).catch(reason => {
                console.error("Notification error: ", reason)
            });
        }


    };

    return {
        scheduleNotification
    };
}