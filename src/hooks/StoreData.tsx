import {Plugins} from '@capacitor/core';
import {CacheFile} from "./CacheFile";
import {WordApi} from "./GenerateApiWord";

const {Storage} = Plugins;

export function StoreData() {

// Save word in storage
    const {
        getPictureInCache
    } = CacheFile();

    const storeObject = async (key: string, valueObject: WordApi) => {
        try {
            const word = JSON.parse(JSON.stringify(valueObject))
            if (word.photo !== undefined) {
                //Do not cache picture source because it's saved on FS
                word.photo.base64 = ""
            }
            await Storage.set({
                key: key,
                value: JSON.stringify(word)
            });
        } catch (e) {
            console.error("Error saving in cache: ", e.toString())
        }
    }

    /**
     * Get word data in lexicon with image
     * @param key word spelling
     */
    const getObject = async (key: string) => {
        const ret = await Storage.get({key: key});
        if (ret.value != null) {
            const objectValue = JSON.parse(ret.value);
            const pictureFile = await getPictureInCache(key);
            objectValue.photo["base64"] = `data:image/jpeg;base64,${pictureFile.data}`
            return objectValue
        }
    }

    const removeObject = async (key: string) => {
        await Storage.remove({key: key});
    }

    const keys = async () => {
        const {keys} = await Storage.keys();
        return keys;
    }

    const validWordKeys = async () => {
        const {keys} = await Storage.keys()
        let validKeys: string[] = []
        let key: string
        for (key of keys) {
            const ret = await Storage.get({key: key})
            if (ret.value != null && key !== "indexCurrentSession" && key !== "indexCurrentLearning") {
                const {value} = ret;
                const word = JSON.parse(value)
                if (word.error === -1) {
                    validKeys.push(key)
                }
            }
        }
        return validKeys
    }

    const clear = async () => {
        await Storage.clear();
    }

    const storeIndexCurrentSession = async (indexCurrentSession: number) => {
        try {
            await Storage.set({
                key: "indexCurrentSession",
                value: JSON.stringify(indexCurrentSession)
            });
        } catch (e) {
            console.error("Error saving index Current Session in cache: ", e.toString())
        }

        try {
            Storage.get({ key: 'store' }).then(res => {
               let new_data; 
                if (res.value == null){
                    new_data = [1]
                }
                else{
                    let val = JSON.parse(res.value)
                    new_data = [...val, indexCurrentSession]

                }

                Storage.set({
                    key: "store",
                    value: JSON.stringify(new_data)
                });
            })
            
        } catch (e) {
            console.error("Error saving index Current Session in cache: ", e.toString())
        }
    }
    

    /**
     * Get current index
     */
    const getIndexCurrentSession = async () => {
        const ret = await Storage.get({key: "indexCurrentSession"});
        if (ret.value != null) {
            return Number(ret.value)
        }
        return 0
    }

    /**
     * Get current index
     */
    const getIndexCurrentLearning = async () => {
        const ret = await Storage.get({key: "indexCurrentLearning"});
        if (ret.value != null) {
            return Number(ret.value)
        }
        return 0
    }

    const storeIndexCurrentLearning = async (indexCurrentLearning: number) => {
        try {
            await Storage.set({
                key: "indexCurrentLearning",
                value: JSON.stringify(indexCurrentLearning)
            });
        } catch (e) {
            console.error("Error saving index Current Session in cache: ", e.toString())
        }
    }
    /**
     * Get isLms
     */
    const getIsLmsEmbeddedApp = async () => {
        const ret = await Storage.get({key: "isLmsEmbeddedApp"});
        if (ret.value != null) {
            return Boolean(ret.value)
        }
        return false
    }
 
    const storeIsLmsEmbeddedApp = async (isLmsEmbeddedApp: boolean) => {
        try {
            await Storage.set({
                key: "isLmsEmbeddedApp",
                value: JSON.stringify(isLmsEmbeddedApp)
            });
        } catch (e) {
            console.error("Error saving isLmsEmbeddedApp in cache: ", e.toString())
        }
    }

    return {
        storeObject,
        getObject,
        removeObject,
        getWordsInLexicon: keys,
        getValidWordsInLexicon: validWordKeys,
        clear,
        getIndexCurrentSession,
        storeIndexCurrentSession,
        getIndexCurrentLearning,
        storeIndexCurrentLearning,
        getIsLmsEmbeddedApp,
        storeIsLmsEmbeddedApp
    };
}
