import {useEffect} from 'react';
import {analytics} from "firebase";

import firebaseConfig from '../creds/firebase.prod.config.json';
import {initializeApp} from "firebase";

// Initialize Firebase
initializeApp(firebaseConfig);
analytics();

export function DataAnalytics() {
    useEffect(() => {
            // your post layout code (or 'effect') here.
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const logEvent = async (eventName: string, object: object) => {
        try {
            await analytics().logEvent(eventName);
        }
        catch (e) {
            console.error('Error Firebase analytics: ', e.message);
        }
        return eventName;
    };

    return {
        logEvent
    };
}