import {useEffect} from 'react';
import {base64FromPath, useFilesystem} from "@ionic/react-hooks/filesystem";
import {FileReadResult, FilesystemDirectory} from "@capacitor/core";
import {Photo} from "./GenerateApiWord";

export function CacheFile() {
    useEffect(() => {
            // your post layout code (or 'effect') here.
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const {writeFile, readFile} = useFilesystem();

    const savePictureInCache = async (url: string, fileName: string): Promise<Photo> => {
        const base64Data = await base64FromPath(url!);
        const option = {
            path: fileName,
            data: base64Data,
            directory: FilesystemDirectory.Data
        }
        await writeFile(option);

        // Use webPath to display the new image instead of base64 since it's
        // already loaded into memory
        return {
            filePath: FilesystemDirectory.Data + "/" + fileName,
            url: url,
            base64: base64Data
        };
    };

    /**
     * Get picture on FS
     * @param fileName key of the picture
     */
    const getPictureInCache = async (fileName: string): Promise<FileReadResult> => {
        const option = {
            path: fileName,
            directory: FilesystemDirectory.Data
        }
        return readFile(option)
    };

    return {
        savePictureInCache,
        getPictureInCache
    };
}