import {useEffect} from 'react';
import {GenerateApiWord} from "./GenerateApiWord"

import axios from "axios";

export interface WordList {
    words: string[];
    length: number;
}

export function UpdateLexiconFromApi() {

    const {updateLexicon} = GenerateApiWord();

    useEffect(() => {
            // your post layout code (or 'effect') here.

        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const requestApiForLexicon = async () => {
        try {
            const secureId = 3294
            const responseWpLexicon = await axios.get("http://localhost/wp-json/wp/v2/users/me?secure=" + secureId)
            updateLexicon(responseWpLexicon.data.bw_lexique_mots)
        } catch (e) {
            console.error("Error api: ", e);
        }
    };

    return {
        requestApiForLexicon
    };
}