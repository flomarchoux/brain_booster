import {useEffect} from 'react'
import axios from 'axios'
import {StoreData} from "./StoreData"
import {CacheFile} from "./CacheFile"
import {DataAnalytics} from "./DataAnalytics"
import {wordinkMaxWordLength, wordinkTimeout} from '../resources/json/Constant.json'

export interface WordApi {
    spelling: string
    definition?: string
    photo?: Photo
    wordRandomList?: string[]
    quiz: string
    brainBoosterScore: number
    brainBoosterIndexSession: number
    brainBoosterSessionLearned: boolean
    error: number
}

export interface Photo {
    filePath: string
    url?: string
    base64?: string
}

let wordList: string[] = []
let lexiconSaved: boolean = false

export function GenerateApiWord() {

    useEffect(() => {
            // your post layout code (or 'effect') here.
        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    // Save word in storage
    const {
        savePictureInCache
    } = CacheFile();

    // Save word in storage
    const {
        storeObject,
        getWordsInLexicon,
        storeIsLmsEmbeddedApp
    } = StoreData();

    // Analytics
    const {
        logEvent
    } = DataAnalytics();

    /**
     * When PWA is encaspulated into BW WP LMS
     * We call the
     * @param lmsLexiconWordList
     */
    const saveLexicon = async (lmsLexiconWordList?: string, callback?: () => void) => {
        if (lexiconSaved || lmsLexiconWordList === null || lmsLexiconWordList === undefined || lmsLexiconWordList === "lms-lexicon-empty") {
            return
        }

        //If we reach this line, app should be embedded in BW LMS
        storeIsLmsEmbeddedApp(true)

        const wordList: string[] = lmsLexiconWordList.split("&")
        updateLexicon(wordList, callback)
        lexiconSaved = true
    }

    /**
     * Add lexicon in users
     * @param lexicon to add
     */
    const updateLexicon = async (lexicon: string[], callback?: () => void) => {
        let word: string, i: number = 0
        const wordsInLexicon: string[] = await getWordsInLexicon()
        for (word of lexicon) {
            if (word && !wordsInLexicon.includes(word?.toLowerCase())) {
                if (i === 0) {
                    console.log("Index 0 update with callback")
                    requestApi(word?.toLowerCase()).then(value => {
                        if (callback)
                            callback()
                    })
                } else {
                    await setTimeout(requestApi,
                        wordinkTimeout * i, word?.toLowerCase()
                    )
                }
                i++
            }
        }
    };

    /**
     * Set a word in lexicon
     * @param word to set, if empty will set a random word
     */
    const requestApi = async (word?: string) => {
        logEvent("LearnRandomCardUserGenerated", {})
        try {
            const responseWordList = await axios.get("https://api.wordnik.com/v4/words.json/randomWords",
                {
                    params: {
                        api_key: "r1bdcw3ejvqxln2cy2eszqxup6vfanlp2s3a6jyugw7gucrdl",
                        hasDictionaryDef: true,
                        minCorpusCount: 75000,
                        minDictionaryCount: 5,
                        limit: 4,
                        minLength: 1,
                        maxLength: wordinkMaxWordLength
                    }
                }
            )

            const wordRandomList = responseWordList.data
            let wordRandom: string = wordRandomList[0].word
            if (word) {
                wordRandomList[0].word = word
                wordRandom = word
            } else {
                console.log("Generate Random Word: ", wordRandom)
            }

            const responseDefinitions = await axios.get("https://api.wordnik.com/v4/word.json/" + wordRandom + "/definitions",
                {
                    params: {
                        api_key: "r1bdcw3ejvqxln2cy2eszqxup6vfanlp2s3a6jyugw7gucrdl",
                        limit: 1,
                        sourceDictionaries: "wiktionary",
                        useCanonical: true
                    }
                }
            )
            //Pix
            let wordDefinition: string = responseDefinitions.data[0].text?.replace(/<xref>/g, "")?.replace(/<\/xref>/g, ""),
                wordPictureUrl: string;

            //PixaBay
            wordPictureUrl = await getPixabay(wordRandom)

            //Unsplash if pixabay not found
            if (wordPictureUrl === undefined) {
                wordPictureUrl = await getPixUnsplash(wordRandom)
            }

            const photo: Photo = await savePictureInCache(wordPictureUrl, wordRandom)

            //Set random word list
            wordList = []
            wordRandomList.forEach(setWordList)
            wordList.sort(function (a, b) {
                return 0.5 - Math.random()
            });

            const newWord = {
                spelling: wordRandom,
                definition: wordDefinition,
                photo: photo,
                wordRandomList: wordList,
                quiz: wordRandom.replace(/(.)/g, "-"),
                brainBoosterScore: 1,
                brainBoosterIndexSession: 1,
                brainBoosterSessionLearned: false,
                error: -1
            };

            await storeObject(newWord.spelling, newWord)

            return newWord

        } catch (error) {
            if (error.response) {
                // Request made and server responded
                console.error('Error request Random: ', error.response.status);
                //If word not found, do not request this word again save it with error status
                if (error.response.status === 404 && word) {
                    const errorWord = {
                        spelling: word,
                        definition: "",
                        photo: undefined,
                        wordRandomList: undefined,
                        quiz: "",
                        brainBoosterScore: 0,
                        brainBoosterIndexSession: 0,
                        brainBoosterSessionLearned: false,
                        error: error.response.status
                    };
                    await storeObject(errorWord.spelling, errorWord)
                }
            } else if (error.request) {
                // The request was made but no response was received
                console.error(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.error('Error while request Random: ', error.message);
            }
        }
    };

    function setWordList(word: any) {
        wordList.push(word.word)
    }

    async function getPixabay(wordRandom: string) {
        //PixaBay
        try {
            let responsePix = await axios.get("https://pixabay.com/api/",
                {
                    params:
                        {
                            key: "16425517-8b8f0c7a8a0e0e055a68f6b9b",
                            q: wordRandom
                        }
                }
            )
            if (responsePix.data.hits[0]) {
                return responsePix.data.hits[0].largeImageURL
            }
        } catch (e) {
            console.error("Error getting pix on pixabay", e.toString())
        }
    }

    async function getPixUnsplash(wordRandom: string) {
        try {
            console.log("No image found on PixaBay");
            const responsePix = await axios.get("https://api.unsplash.com/search/photos/",
                {
                    params:
                        {
                            client_id: "B5AyIHLGnr8DJsF_IQaKEgeoUycVCHdHoQeW7Ti_9DY",
                            query: wordRandom
                        }
                }
            )

            if (responsePix.data.results[0]) {
                return responsePix.data.results[0].urls.regular
            } else {
                console.log("No image found on UnSplash");
            }
        } catch (e) {
            console.error("Error getting pix on unsplash", e.toString())
        }
    }

    return {
        requestApi,
        saveLexicon,
        updateLexicon
    };
}
