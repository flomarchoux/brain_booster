import {useEffect} from 'react'
import {Capacitor, Plugins} from '@capacitor/core'

const isOrientationAvailable = Capacitor.isPluginAvailable('ScreenOrientation')

export function ScreenOrientationLocker() {
    useEffect(() => {

        },
        // array of variables that can trigger an update if they change. Pass an
        // an empty array if you just want to run it once after component mounted.
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [])

    const lockOrientation = async () => {
        if(isOrientationAvailable){
            const {ScreenOrientation} = Plugins
            ScreenOrientation.lock(ScreenOrientation.ORIENTATIONS.PORTRAIT)
        }
    }

    const unlockOrientation = async () => {
        // ScreenOrientation.unlock();
    }

    return {
        lockOrientation,
        unlockOrientation
    };
}