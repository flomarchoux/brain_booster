import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';
import {caretForwardCircle, rocket, lockClosed, addCircleOutline, saveSharp, analytics} from 'ionicons/icons';
import Connection from './pages/Connection';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
/* Theme variables */
import './theme/variables.css';
/* BW variables */
import './theme/bw.css';
import {useTranslation} from "react-i18next";
import LearnLexiconPage from "./pages/learn/LearnLexiconPage";
import QuizPage from "./pages/learn/BrainBoosterPage";
import {LocalNotification} from "./hooks/LocalNotification";
import Graphic from "./pages/learn/Graphic";

import CookieController from "./components/CookieController/CookieController";

import './app.css';
import {SpecialCss} from "./hooks/SpecialCss";
import Cookies from "universal-cookie";
import Register from './pages/Register';

const server = "http://192.168.1.46:8080";

const App: React.FC = () => {
    // @ts-ignore
    const cookies = new Cookies();

    function disconnect() {
        //@ts-ignore
        cookies.remove("brain_booster_id", {path: "/"});
        //@ts-ignore
        cookies.remove("brain_booster_email", {path: "/"});
        //@ts-ignore
        cookies.remove("brain_booster_hash", {path: "/"});
        //@ts-ignore
        alert('Vous êtes bien déconnecté')
        localStorage.clear();
        //@ts-ignore
        window.location = "/"
    }

    //@ts-ignore
    function save(id) {

        let length = Object.entries(localStorage).length;
        IndexedDB();
        for (let i = 0; i < length; i++) {
            let key = Object.entries(localStorage)[i][0];
            let data = Object.entries(localStorage)[i][1];
            console.log(JSON.stringify({id, key, data}));
            fetch(server + "/setClientSave", {
                method: 'POST',
                headers: {
                    'content-type': 'application/json',
                },
                body: JSON.stringify({id, key, data})
            })
                .then(response => {
                })
        }

    }

    function IndexedDB() {
        const request = indexedDB.open("Disc", 1);
        request.onerror = (event) => {
            console.log("HELP ME MARIO !!")
        };
        request.onsuccess = (event) => {
            //@ts-ignore
            const db = event.target.result;
            getAllValuesFromDb(db)
        };
    }

    //@ts-ignore
    function getAllValuesFromDb(db) {
        const txn = db.transaction("FileStorage", "readonly");
        const objectStore = txn.objectStore("FileStorage");
        //@ts-ignore
        objectStore.openCursor().onsuccess = (event) => {
            let cursor = event.target.result;
            if (cursor) {
                console.log(cursor.value);
                let user_id = cookies.get('brain_booster_id');
                let dataidb = cursor.value;
                console.log(user_id);
                fetch(server + "/setClientIDB", {
                    /*mode : "no-cors",*/
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                    },
                    body: JSON.stringify({user_id, dataidb})
                })
                cursor.continue();
            }
        }
        txn.oncomplete = function () {
            db.close();
        }
    }

    const {t} = useTranslation()
    LocalNotification().scheduleNotification(t("notificationTitle"), t("notificationContent"))
    const {cssSpecial} = SpecialCss()
    if (CookieController() === true) {
        return (
            <IonApp className="app-no-margin">
                <IonReactRouter>
                    <IonTabs className={cssSpecial}>
                        <IonRouterOutlet>
                            <Redirect exact from="/" to="/learn/quiz"/>
                            <Route path="/connection" component={Connection} exact={true}/>
                            <Route path="/learn/lexicon" component={LearnLexiconPage} exact={true}/>
                            <Route path="/learn/lexicon/:lmslexicon" component={LearnLexiconPage} exact={true}/>
                            <Route path="/learn/quiz" component={QuizPage} exact={true}/>
                            <Route path="/learn/quiz/:lmslexicon" component={QuizPage} exact={true}/>
                            <Route path="/learn/graph" component={Graphic} exact={true}/>
                        </IonRouterOutlet>
                        <IonTabBar slot="bottom" selectedTab="quiz">
                            <IonTabButton tab="quiz" href="/learn/quiz/">
                                <IonIcon icon={rocket}/>
                                <IonLabel>{t('quiz')}</IonLabel>
                            </IonTabButton>
                            <IonTabButton tab="lexicon" href="/learn/lexicon">
                                <IonIcon icon={caretForwardCircle}/>
                                <IonLabel>{t('memorize')}</IonLabel>
                            </IonTabButton>
                            <IonTabButton>
                                <IonIcon icon={lockClosed} onClick={() => disconnect()}/>
                                <IonLabel onClick={() => disconnect()}>{t('Disconnect')}</IonLabel>
                            </IonTabButton>
                            <IonTabButton>
                                <IonIcon icon={saveSharp} onClick={() => save(cookies.get("brain_booster_id"))}/>
                                <IonLabel onClick={() => save(cookies.get("brain_booster_id"))}>{t('Save')}</IonLabel>
                            </IonTabButton>
                            <IonTabButton tab="graph" href="/learn/graph">
                                <IonIcon icon={analytics}/>
                                <IonLabel>{t('Graphique')}</IonLabel>
                            </IonTabButton>
                        </IonTabBar>
                    </IonTabs>
                </IonReactRouter>
            </IonApp>
        );
    }
    return (
        <IonApp className="app-no-margin">
            <IonReactRouter>
                <IonTabs className={cssSpecial}>
                    <IonRouterOutlet>
                        <Redirect exact from="/" to="/learn/quiz"/>
                        <Route path="/connection" component={Connection} exact={true}/>
                        <Route path="/register" component={Register} exact={true}/>
                        <Route path="/learn/lexicon" component={LearnLexiconPage} exact={true}/>
                        <Route path="/learn/lexicon/:lmslexicon" component={LearnLexiconPage} exact={true}/>
                        <Route path="/learn/quiz" component={QuizPage} exact={true}/>
                        <Route path="/learn/quiz/:lmslexicon" component={QuizPage} exact={true}/>
                        <Route path="/learn/graph" component={Graphic} exact={true}/>
                    </IonRouterOutlet>
                    <IonTabBar slot="bottom" selectedTab="quiz">
                        <IonTabButton tab="quiz" href="/learn/quiz/">
                            <IonIcon icon={rocket}/>
                            <IonLabel>{t('quiz')}</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="lexicon" href="/learn/lexicon">
                            <IonIcon icon={caretForwardCircle}/>
                            <IonLabel>{t('memorize')}</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="connection" href="/connection">
                            <IonIcon icon={lockClosed}/>
                            <IonLabel>{t('connection')}</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="register" href="/register">
                            <IonIcon icon={addCircleOutline}/>
                            <IonLabel>{t('register')}</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="graph" href="/learn/graph">
                                <IonIcon icon={analytics}/>
                                <IonLabel>{t('Graphique')}</IonLabel>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonReactRouter>
        </IonApp>
    );
}

export default App;

