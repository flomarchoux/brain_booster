import React from 'react';
import {IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import {useTranslation} from "react-i18next";
import RegisterContainer from "../components/Register/RegisterContainer";


const Register: React.FC = () => {
    const {t} = useTranslation();

    return (
        <IonPage>
            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{t('register')}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <RegisterContainer/>
            </IonContent>
        </IonPage>
    );
};

export default Register;
