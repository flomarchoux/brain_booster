import React from 'react';
import {IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import {useTranslation} from "react-i18next";
import ConnectionContainer2 from "../components/connection/ConnectionContainer";


const Connection: React.FC = () => {
    const {t} = useTranslation();

    return (
        <IonPage>
            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{t('connection')}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <ConnectionContainer2/>
            </IonContent>
        </IonPage>
    );
};

export default Connection;
