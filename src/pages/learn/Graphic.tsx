import React from 'react';
import { useState } from 'react';
import { IonPage } from '@ionic/react';
import { RouteComponentProps } from "react-router";
import { Doughnut, Line, Bar } from 'react-chartjs-2';
import './graphic.css';


interface GraphicProps extends RouteComponentProps {
    lmslexicon: string;
}


const Graphic: React.FC<GraphicProps> = ({  }) => {
    const [val, setVal] = useState(localStorage.getItem('_cap_store'))
    const jours_connect = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche']

    //@ts-ignore
    function valToNumber(array) {
        //@ts-ignore
        let value = [];
        for (let i = 1; i < array?.length - 1; i++) {
            //@ts-ignore
            if (array[i].length !== 0 && array[i] !== ",") {
                if (array[i + 1] !== "," && array[i + 1] !== "]") {
                    //@ts-ignore
                    value.push(Number(array[i] + array[i + 1]))
                }
                else {
                    //@ts-ignore
                    value.push(Number(array[i]))
                }
            }
        }
        //@ts-ignore
        return value
    }

    return (
        <IonPage>
            <div>
                {/* ########################Graphique 1#######################*/}

                <div id="graph1">
                    <Line
                        data={{
                            labels: jours_connect,
                            datasets: [
                                {
                                    label: 'Connexion',
                                    data: [8, 14, 21, 36, 49, 70, 50, 60, 82, 74, 56, 42, 14, 52],
                                    backgroundColor:'rgb(238,130,238)',
                                    borderColor: 'rgba(0, 0, 0, 1)',
                                    pointBackgroundColor: 'black',
                                    borderWidth: 3,
                                    lineTension: 0,
                                },
                            ],
                        }}

                        height={300}
                        width={600}

                        options={{
                            maintainAspectRatio: false,
                            legend: {
                                position: 'bottom',
                            },
                            title: {
                                display: true,
                                text: 'Taux de connexion',
                                fontColor: 'black',
                                fontSize: '18',
                                position: 'top',
                            },
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                        }}
                    ></Line>
                </div>

                {/* ########################Graphique 2#######################*/}

                <div id="graph2">
                    <Bar
                        data={{
                            labels: jours_connect,
                            datasets: [
                                {
                                    label: 'Nombre de mots',
                                    data: valToNumber(val),
                                    backgroundColor: 'rgba(255, 99, 132, 1)',
                                    borderColor: 'rgba(0, 0, 0, 0.7)',
                                    pointBackgroundColor: 'black',
                                    borderWidth: 3,
                                    lineTension: 0,
                                },
                                {
                                    label: 'Nombre mot déjà appris',
                                    data: [1, 4, 8, 11, 15, 17, 21],
                                    backgroundColor: 'grey',
                                    borderColor: 'rgb(192,192,192,0.7)',
                                    pointBackgroundColor: 'black',
                                    lineTension: 0,
                                },
                            ]
                        }}
                        height={300}
                        width={600}

                        options={{
                            maintainAspectRatio: false,
                            legend: {
                                position: 'bottom',
                            },
                            title: {
                                display: true,
                                text: 'Nombre de mots appris au cours du temps',
                                fontColor: 'black',
                                fontSize: '18',
                                position: 'top',
                            },
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                        }}
                    ></Bar>
                </div>

                {/* ########################Graphique 3#######################*/}

                <div id="graph3">
                    <Bar
                        data={{
                            labels: ['A1', 'A2', 'B1', 'B2', 'C1', 'C2'],
                            datasets: [
                                {
                                    label: 'Nombre de mots',
                                    data: [10, 20, 30, 50, 70, 100],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.8)',
                                        'rgba(54, 162, 235, 0.8)',
                                        'rgba(255, 206, 86, 0.8)',
                                        'rgba(75, 192, 192, 0.8)',
                                        'rgba(153, 102, 255, 0.8)',
                                        'rgba(255, 159, 64, 0.8)'
                                    ],
                                    borderColor: [
                                        'rgba(200, 99, 132, 1)',
                                        'rgba(54, 162, 255, 1)',
                                        'rgba(255, 250, 86, 1)',
                                        'rgba(75, 192, 100, 1)',
                                        'rgba(200, 102, 255, 1)',
                                        'rgba(255, 200, 64, 1)'
                                    ],
                                    borderWidth: 3,
                                    hoverBorderColor: 'black',
                                },

                            ]
                        }}
                        height={300}
                        width={300}

                        options={{
                            maintainAspectRatio: false,
                            legend: {
                                position: 'bottom',
                            },

                            title: {
                                display: true,
                                text: 'Niveaux',
                                fontColor: 'black',
                                fontSize: '18',
                                position: 'top',
                            },
                            scales: {
                                yAxes: [
                                    {
                                        ticks: {
                                            beginAtZero: true,
                                        },
                                    },
                                ],
                            },
                        }}
                    ></Bar>
                </div>

                {/* ########################Graphique 4#######################*/}

                <div id="graph4">
                    <Doughnut
                        data={{
                            labels: ['Avatar Apprenti', 'Avatar Débutant', 'Avatar Intermediaire', 'Avatar Pro', 'Avatar Elite', 'Avatar Maître'],
                            datasets: [
                                {
                                    data: [5, 10, 30, 55, 85, 100],
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.8)',
                                        'rgba(54, 162, 235, 0.8)',
                                        'rgba(255, 206, 86, 0.8)',
                                        'rgba(75, 192, 192, 0.8)',
                                        'rgba(153, 102, 255, 0.8)',
                                        'rgba(255, 159, 64, 0.8)'
                                    ],
                                    borderColor: [
                                        'rgba(200, 99, 132, 1)',
                                        'rgba(54, 162, 255, 1)',
                                        'rgba(255, 250, 86, 1)',
                                        'rgba(75, 192, 100, 1)',
                                        'rgba(200, 102, 255, 1)',
                                        'rgba(255, 200, 64, 1)'
                                    ],
                                    borderWidth: 3,
                                    hoverBorderColor: 'black',
                                },

                            ]
                        }}
                        height={300}
                        width={300}

                        options={{
                            maintainAspectRatio: false,
                            legend: {
                                position: 'bottom',
                            },

                            title: {
                                display: true,
                                text: 'Récompense de connexion',
                                fontColor: 'black',
                                fontSize: '18',
                                position: 'top',
                            },
                        }}
                    ></Doughnut>
                </div>
            </div>
        </IonPage>
    );

};
export default Graphic;
