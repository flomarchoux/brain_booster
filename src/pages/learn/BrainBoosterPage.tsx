import React from 'react';
import {IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import {useTranslation} from "react-i18next";
import LearnBrainBoosterContainer from "../../components/learn/LearnBrainBoosterContainer";
import {RouteComponentProps} from "react-router";
import {ScreenOrientationLocker} from "../../hooks/ScreenOrientationLocker";

interface QuizPageProps extends RouteComponentProps<{
    lmslexicon: string;
}> {
}

const LearnLexiconPage: React.FC<QuizPageProps> = ({match}) => {
    const {t} = useTranslation()
    ScreenOrientationLocker().lockOrientation()
    return (
        <IonPage>
            <IonContent>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{t('quiz')}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <LearnBrainBoosterContainer lmslexicon={match.params.lmslexicon}/>
            </IonContent>
        </IonPage>
    );
};
export default LearnLexiconPage;