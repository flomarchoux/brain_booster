import React from 'react';
import {IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import {useTranslation} from "react-i18next";
import LearnLexiconContainer from "../../components/learn/LearnLexiconContainer";
import {RouteComponentProps} from "react-router";
import {ScreenOrientationLocker} from "../../hooks/ScreenOrientationLocker";

interface LearnLexiconPageProps extends RouteComponentProps<{
    lmslexicon: string;
}> {
}

const LearnLexiconPage: React.FC<LearnLexiconPageProps> = ({match}) => {
    const {t} = useTranslation()
    ScreenOrientationLocker().lockOrientation()
    return (
        <IonPage>
            <IonContent fullscreen={true} class="brain-booster-no-scroll-ion-content">
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">{t('memorize')}</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <LearnLexiconContainer lmslexicon={match.params.lmslexicon}/>
            </IonContent>
        </IonPage>
    );
};
export default LearnLexiconPage;